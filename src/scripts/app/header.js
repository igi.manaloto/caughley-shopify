$(document).ready(function() {
  "use strict";
  var mobBreakpoint = 768;
  var $theHeaderMenu = $('.the-header-menu');
  var $theMenuUl = $theHeaderMenu.find("> ul");

  $('.has-inner-submenu').closest('.has-submenu').addClass('has-inner-submenu');

  // Prevent parent links from proceeding so we can view children items on mobile!
  $('.has-submenu, .has-inner-submenu').on('click', ' > a', function(e){
    e.preventDefault();
    if (!$(window).width() > mobBreakpoint) {
      window.location = $(this).attr('href');
    }
  });


  //Checks if li has sub (ul) and adds class for toggle icon - just an UI
  var $innerUlNoChildren = $theMenuUl.find('> li > ul:not(:has(ul))')
  $innerUlNoChildren.find('li').contents().unwrap();
  $innerUlNoChildren.wrap('<div class="submenu-no-children"><div class="narrow-container"><div class="submenu-no-children-columns"></div></div></div>').contents().unwrap();

  //If width is more than mobBreakpoint dropdowns are displayed on hover
  $theMenuUl.find('> li').hover(function(e) {
    if ($(window).width() > mobBreakpoint) {
      $(this).children("ul, .submenu-no-children").stop(true, false).toggleClass('is-active');
      e.preventDefault();
    }
  });

  ////If width is less or equal to mobBreakpoint dropdowns are displayed on click
  $theMenuUl.find('.has-submenu > a, .has-inner-submenu > a').click(function() {
    if ($(window).width() <= mobBreakpoint) {
      $(this).next().stop(true,false).toggleClass('is-active');
    }
  });


  //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story
  $(".mobile-menu-toggle").click(function(e) {
    e.preventDefault();
    // Close any timber menus
    // timber.RightDrawer.close();
    $(this).toggleClass('is-active');
    $theHeaderMenu.toggleClass('is-fullscreen');
    $('html').toggleClass('is-clipped');
    $(this).find('.i-icon').toggleClass('navicon').toggleClass('close');
  });

});
