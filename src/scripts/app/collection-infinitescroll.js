$(document).ready(function(){
  // The pagination button that houses the require uri for the next page of collections
  var $next = $('#collection-next');

  // Serves as the lock while we process an ajax request
  var triggered = false;

  // Load products via ajax, making a requirest to collections?view=raw
  var ajaxLoadProducts = function() {
    if($(document).scrollTop() > $('.collection-grid').outerHeight() && $next.length > 0 && !triggered) {
      var nextUrl = $next.attr("href");
      console.log(nextUrl);
      $.ajax({
          type: 'GET',
          url: nextUrl,
          beforeSend: function() {
            // We set triggered to true so we don't keep on requesting
            triggered = true;
            $next.addClass('is-loading');
          },
          success: function(data) {
            // Add a 500ms delay first for a smoother loading experience
            setTimeout(function(){
              $next.removeClass('is-loading');
              // Append the result set to the grid
              var $responseHTML = $(data);

              // Append and animate the new items
              $responseHTML.appendTo($(".collection-grid")).css('opacity', 0).animate({
                opacity: 1
              });

              // Remove the next button from the response, after it has been inserted
              $collectionNext = $(".collection-grid").find('#next-pagination-link');

              // Check for the existence of a pagination next button
              if($collectionNext.length){
                // Assign the data url to the button's href
                // Use attr instead of data to ensure that we
                // can access unbound elements' attributes
                $next.attr('href', $collectionNext.attr('data-url'));
                // Remove it from the page as we no longer need it
                $collectionNext.remove();
              } else {
                // We don't need the next button anymore, so we'll just remove it
                $next.remove();
                $next = -1;
              }
              // We can reset triggered now that we've appended the results to the page
              triggered = false;
            }, 250);
          },
          dataType: "html"
      });
    }
  }


  $(window).on('scroll', function(e) {
      ajaxLoadProducts();
  }, 250);
});
