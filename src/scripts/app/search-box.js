$(document).ready(function($) {
  'use strict';

  var $searchBox = $('.the-search-box');
  var $toggleSearch = $('.search-toggle');
  var $searchInput = $searchBox.find('.input');

  $toggleSearch.on('click', function(e){
    e.preventDefault();
    $searchBox.toggleClass('is-active');
    if($searchBox.hasClass('is-active')){
      // Add a timeout
      setTimeout(function(){
        $searchInput.focus();
      }, 500)
    }
  });
});
