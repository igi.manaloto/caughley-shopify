$(document).ready(function($) {
  'use strict';
  var $grids = $('.columns.is-multiline.is-masonry');
  $grids.each(function(){
    var $grid = $(this);

    // Because we're using masonry, we have to set heights to the
    // absolutely positioned elements
    var resizeColumnHeights = function(){
      var $columns = $grid.find('.column');
      // Get an array of all element heights
      var elementHeights = $columns.map(function() {
        return $(this).height();
      }).get();

      // Math.max takes a variable number of arguments
      // `apply` is equivalent to passing each height as an argument
      var maxHeight = Math.max.apply(null, elementHeights);

      // Set each height to the max height
      console.log(maxHeight);
      $columns.css({'min-height': maxHeight});
    }

    // Init masonry
    var $masonry = $grid.masonry({
      itemSelector: '.column'
    });

    // Attach imagesLoaded to wait for actual heights to settle
    $masonry.imagesLoaded().progress(function(){
      // Bind resizing to masonry
      $masonry.on('layoutComplete', resizeColumnHeights());

      // Call resize
      $grid.masonry('layout');
    });
  });
});
